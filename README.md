A simple way to add a log activity to an Android app. Convenient for debugging or logging useful information for the user. The log can be sent to another app on the device (clipboard, email app, ...).


# ADD AN ENTRY #

To add a log entry, use LogApp.add(tag, sayWhat);
where both tag and sayWhat are String.

E.g.,
```
LogApp.add("IMPORTANT", "Tashtego and Ahab have spotted the whale.");
...
LogApp.add("PUN", "Fishermen are reel men.");
```

would log the information appearing like this to the user in the LogAppActivity:
```
03:13:59 IMPORTANT
Tashtego and Ahab have spotted the whale.
03:14:00 PUN
Fishermen are reel men.
```

# CLEAR THE LOG #

To clear the log, use LogApp.clear();


# LogApp LISTENER #

To be informed of an add event or a clear event, a class can use the public interface LogApp.LogAppListener and implement the callback methods onEntryAdded and onClear.


# CREATE A LogAppActivity #

To create an Intent to the LogAppActivity, use LogApp.getLogAppActivityIntent(context); 
where context is a Context.

To start an activity LogAppActivity with the logged information, use:
```
Intent intent = LogApp.getLogAppActivityIntent(context);
startActivity(intent);
```

Do not forget to add to AndroidManifest.xml:
```
<application ...>
...
<activity android:name=".LogApp$LogAppActivity" />
...
</application>
```

Change package eu.app.utils and import eu.app.mainpackage.R to your needs.


# EXAMPLE OF INTEGRATION #

 * add toolbar.xml in /res/layout
 * add log_app.xml in /res/layout
 * add log_app_menu.xml in /res/menu
 * add the LogApp$LogAppActivity to AndroidManifest.xml.

E.g., to add the functionality to a menu:
```
     <item android:id="@+id/open_log_app_activity"
           android:title="Show LogApp Activity"
           android:visible="@bool/log_app_or_not"
           app:showAsAction="never" />
```
then, in onOptionsItemSelected use:
```
     case R.id.open_log_app_activity:
          Intent intent = LogApp.getLogAppActivityIntent(context);
          startActivity(intent);
          return true;
```

Use LogApp.add("INFO", "Information.") in your code to log information.
Use LogApp.clear() to clear the log.