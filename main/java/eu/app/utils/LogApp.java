package eu.app.utils;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import eu.app.mainpackage.R;


/**
 * Created by Nicolas Troquard on 09/09/16.
 *
 * A simple way to add a log activity to an Android app.
 * Convenient for debugging or logging useful information for the user.
 * The log can be sent to another app on the device (clipboard, email app, ...).
 *
 *
 * ***** ADD AN ENTRY
 *
 * To add a log entry, use LogApp.add(tag, sayWhat);
 * where both tag and sayWhat are String.
 *
 * E.g.,
 *      LogApp.add("IMPORTANT", "Tashtego and Ahab have spotted the whale.");
 *      ...
 *      LogApp.add("PUN", "Fishermen are reel men.");
 *
 * would log the information appearing like this to the user in the LogAppActivity:
 *
 *      03:13:59 IMPORTANT
 *      Tashtego and Ahab have spotted the whale.
 *      03:14:00 PUN
 *      Fishermen are reel men.
 *
 *
 * ***** CLEAR THE LOG
 *
 *  To clear the log, use LogApp.clear();
 *
 *
 * ***** LogApp LISTENER
 *
 *  To to informed of an add event or a clear event, a class can use the public interface
 *  LogApp.LogAppListener and implement the callback methods onEntryAdded and onClear.
 *
 *
 * ***** CREATE A LogAppActivity
 *
 * To create an Intent to the LogAppActivity, use LogApp.getLogAppActivityIntent(context);
 * where context is a Context.
 *
 * To start an activity LogAppActivity with the logged information, use:
 *
 *      Intent intent = LogApp.getLogAppActivityIntent(context);
 *      startActivity(intent);
 *
 *  Do not forget to add to AndroidManifest.xml:
 *
 *      <application ...>
 *      ...
 *          <activity android:name=".LogApp$LogAppActivity" />
 *      ...
 *      </application>
 *
 * Change package eu.app.utils and import eu.app.mainpackage.R to your needs.
 *
 * ***** EXAMPLE OF INTEGRATION
 *
 * - add toolbar.xml in /res/layout
 * - add log_app.xml in /res/layout
 * - add log_app_menu.xml in /res/menu
 * - add the LogApp$LogAppActivity to AndroidManifest.xml.
 *
 * E.g., to add the functionality to a menu:
 *
 *     <item android:id="@+id/open_log_app_activity"
 *           android:title="Show LogApp Activity"
 *           android:visible="@bool/log_app_or_not"
 *           app:showAsAction="never" />
 *
 * then, in onOptionsItemSelected use:
 *
 *     case R.id.open_log_app_activity:
 *          Intent intent = LogApp.getLogAppActivityIntent(context);
 *          startActivity(intent);
 *          return true;
 *
 * Use LogApp.add("INFO", "Information.") in your code to log information.
 * Use LogApp.clear() to clear the log.
 */
public class LogApp {

    private static List<LogAppListener> listeners = new ArrayList<>();
    private static ArrayList<LogEntry> log = new ArrayList<>();

    public static void add(String tag, String sayWhat) {
        LogEntry newEntry = new LogEntry(tag, sayWhat);
        log.add(newEntry);
        notifyListenersAdd(newEntry);
    }

    public static void clear() {
        log.clear();
        notifyListenersClear();
    }

    private static String getLogString() {
        StringBuilder logBuilder = new StringBuilder();
        for (LogEntry entry : log) {
            logBuilder.append(entry.toString());
            logBuilder.append("\n");
        }
        return logBuilder.toString();
    }

    public static Intent getLogAppActivityIntent(Context context) {
        return new Intent(context, LogApp.LogAppActivity.class);
    }

    private static void addListener(LogAppListener listener) {
        listeners.add(listener);
    }

    private static void removeListener(LogAppListener listener) {
        listeners.remove(listener);
    }

    private static void notifyListenersAdd(LogEntry newEntry) {
        for (LogAppListener listener : listeners)
            listener.onEntryAdded(newEntry);
    }

    private static void notifyListenersClear() {
        for (LogAppListener listener : listeners)
            listener.onClear();
    }



    /*
     * inner INTERFACE (LogAppListener)
     * and
     * inner CLASSES (LogEntry and LogAppActivity)
     */

    public interface LogAppListener {
        void onEntryAdded(LogEntry newEntry);
        void onClear();
    }

    private static class LogEntry {
        private int hour;
        private int minute;
        private int second;
        private String tag;
        private String sayWhat;

        LogEntry(String tag, String sayWhat) {
            this.hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            this.minute = Calendar.getInstance().get(Calendar.MINUTE);
            this.second = Calendar.getInstance().get(Calendar.SECOND);
            this.tag = tag;
            this.sayWhat = sayWhat;
        }

        @Override
        public String toString() {
            String zero = "0";
            String columnzero = ":0";
            String column = ":";
            StringBuilder stringBuilder = new StringBuilder();

            if (hour < 10) stringBuilder.append(zero);
            stringBuilder.append(hour);
            stringBuilder.append((minute < 10)?columnzero:column);
            stringBuilder.append(minute);
            stringBuilder.append((second < 10)?columnzero:column);
            stringBuilder.append(second);

            stringBuilder.append(" ");
            stringBuilder.append(tag);
            stringBuilder.append("\n");

            stringBuilder.append(sayWhat);

            return stringBuilder.toString();
        }
    }


    /*
     *
     * LogAppActivity
     *
     */

    public static class LogAppActivity extends AppCompatActivity implements LogAppListener {

        // We implement LogAppListener so that we can update the activity
        // if and when a new entry is added to LogApp or if it is cleared.

        // This is why we retain an instance of textView.
        TextView textView;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.log_app);

            Toolbar toolbar = (Toolbar) findViewById(R.id.log_app_action_bar);
            setSupportActionBar(toolbar);
            toolbar.setTitle("Log " + getApplicationContext().getString(getApplicationInfo().labelRes));
            setSupportActionBar(toolbar);

            textView = (TextView) findViewById(R.id.log_app_textView);
            textView.setText(LogApp.getLogString());

            // Finally we add this activity to the listeners of LogApp.
            LogApp.addListener(this);
        }

        @Override
        protected void onDestroy() {
            super.onDestroy();
            LogApp.removeListener(this);
        }


        /* MENU */

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.log_app_menu, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();

            if (id == R.id.log_app_action_clear) {
                clearLog();
                return true;
            }
            if (id == R.id.log_app_action_send) {
                send();
                return true;
            }

            return super.onOptionsItemSelected(item);
        }



        /* MENU ACTIONS */

        public void clearLog() {
            LogApp.clear();
        }

        public void send() {
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, LogApp.getLogString());

            try {
                startActivity(Intent.createChooser(sendIntent, "Send to"));
                finish();
            } catch (android.content.ActivityNotFoundException ex) {
                // nothing
            }
        }


        /* LogAppListener methods */

        @Override
        public void onEntryAdded(LogEntry newEntry) {
            textView.append(newEntry.toString() + "\n");
        }

        @Override
        public void onClear() {
            textView.setText("");
        }
    }

}
